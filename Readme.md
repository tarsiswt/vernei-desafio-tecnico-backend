# Exercício Técnico Dev Back Java [SICREDI VERNEI] - Tarsis

## Visão Geral e Arquitetura

A arquitetura interna deste serviço de backend é inspirada na Arquitetura Hexagonal (https://alistair.cockburn.us/hexagonal-architecture/). A Figura abaixo, de autoria de Chris Richardson (http://chrisrichardson.net/post/microservices/general/2019/02/16/whats-a-service-part-1.html), mostra como esta arquitetura é composta.

![alt text](http://chrisrichardson.net/i/what-is-a-service/What_is_a_Service.png "Arquitetura Hexagonal")

No cerne, encontram-se as classes que implementam a lógica e capacidades do negócio. A separação deste código de outros interesses distintos, são, da melhor forma possível, ponderando-se conveniência, ergonomia de desenvolvimento etc, alcançadas através do uso de interfaces. Neste sentido, costuma-se dizer que a direção das dependêncas vão sempre "de fora para dentro", isto é, a lógica de negócio não depende de ninguém mas outras camadas dependem dela.

Quando a camada de negócio precisa acessar um elemento externo como o banco de dados ou uma fila de mensagens, por exemplo, este acesso é feito sempre através de interfaces cujas implementações são fornecidas pelo contêiner de injeção de dependências do Spring Framework.

Os pacotes `dbc.desafio.twt.coop.domain.*.model` contém as classes que representam juntamente esses conceitos de negócio, sendo que `*` significa os três conceitos centrais desta aplicação: `Associado`, `Pauta` e `Voto`.

## Funcionalidades

As funcionalidade solicita foram as seguintes.

* Cadastrar uma nova pauta;
* Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default);
* Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta);
* Contabilizar os votos e dar o resultado da votação na pauta.

A porta de entrada para todos estes requisitos é a API REST encabeçada pela classe `PautaController` que age como ponto de entrada e delega as operações para a camada de negócio.

#### Tarefas bônus

* Tarefa Bônus 1 - Integração com sistemas externos

A integração com o serviço de validação é encontrada em `ServicoDeValidacaoDeCPF`

* Tarefa Bônus 2 - Mensageria e filas

O envio de mensagens para uma fila específica é implementado principalmente na classe `PublicarEncerramentoRabbitMQ`

* Tarefa Bônus 3 - Performance

Há um cenário de teste de carga feito com o Gatling () que pode ser executado pelo comando `mvn gatling:test`. A descrição deste cenário com a DSL do Gatling pode ser encontrada no arquivo `PautaAPIGatlingSimulation`

* Tarefa Bônus 4 - Versionamento da API

A estratégia escolhida para a primeira versão da API é de utilizar um Media Type específico, a saber `application/vnd.dbc.coop.api-v1+json`. As requisições à API REST deve enviar o header `Accept` com este media type e este é o mecanismo pelo qual o cliente informa qual versão da API gostaria de utilizar. Uma das principais vantagens deste método é que a versão do *recurso* identificado pela URI não fica atrelado à versão da API.

## Execução

Todas as dependências da aplicação são gerenciadas pelo Maven. Para gerar o `.jar` executavel utilize o comando `mvn package`. Utilize o diretório onde se encontra o `docker-compose.yml` para executar `docker-compose build` e `docker-compose up` para iniciar um banco de dados MySQL que a aplicação usa como persistência transacional e o outro contêiner com RabbitMQ para envio de mensagens (ver Tarefa Bônus 2), ambos já devidamente configurados.
