package dbc.desafio.twt.coop.application.controller;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.CriarAssociadoCommand;
import dbc.desafio.twt.coop.domain.associado.ports.CriarAssociado;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/associado", produces = "application/vnd.dbc.coop.api-v1+json")
public class AssociadoController {

    private final CriarAssociado criarAssociado;

    public AssociadoController(CriarAssociado criarAssociado) {
        this.criarAssociado = criarAssociado;
    }

    @PostMapping()
    public Associado criarAssociado(@RequestBody AssociadoDTO associadoDTO) {
        CriarAssociadoCommand criarAssociadoCommand = new CriarAssociadoCommand(associadoDTO.nome, associadoDTO.CPF);
        return criarAssociado.handle(criarAssociadoCommand);
    }

    public static class AssociadoDTO {
        private final String nome;
        private final String CPF;

        public AssociadoDTO(String nome, String CPF) {
            this.nome = nome;
            this.CPF = CPF;
        }

        public String getNome() {
            return nome;
        }

        public String getCPF() {
            return CPF;
        }
    }
}
