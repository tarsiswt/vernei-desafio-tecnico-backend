package dbc.desafio.twt.coop.application.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.pauta.model.AbrirSessaoCommand;
import dbc.desafio.twt.coop.domain.pauta.model.CriarPautaCommand;
import dbc.desafio.twt.coop.domain.pauta.ports.AbrirSessao;
import dbc.desafio.twt.coop.domain.pauta.ports.CriarNovaPauta;
import dbc.desafio.twt.coop.domain.voto.model.ContabilizarVotosCommand;
import dbc.desafio.twt.coop.domain.voto.model.ReceberVotoCommand;
import dbc.desafio.twt.coop.domain.voto.ports.ContabilizarVotos;
import dbc.desafio.twt.coop.domain.voto.ports.ReceberVoto;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/pauta", produces = "application/vnd.dbc.coop.api-v1+json")
public class PautaController {

    private final CriarNovaPauta criarNovaPauta;
    private final ContabilizarVotos contabilizarVotos;
    private final AbrirSessao abrirSessao;
    private final ReceberVoto receberVoto;

    public PautaController(CriarNovaPauta criarNovaPauta,
                           ContabilizarVotos contabilizarVotos,
                           AbrirSessao abrirSessao,
                           ReceberVoto receberVoto) {
        this.criarNovaPauta = criarNovaPauta;
        this.contabilizarVotos = contabilizarVotos;
        this.abrirSessao = abrirSessao;
        this.receberVoto = receberVoto;
    }

    @PostMapping(path = "")
    public Pauta novaPauta(@RequestBody CriarPautaCommand criarPauta) {
        return criarNovaPauta.handle(criarPauta);
    }

    @GetMapping(value = "/{pautaId}/contabilizarVotos")
    public Map<Boolean, Long> contabilizarVotos(@PathVariable("pautaId") UUID uuid) {
        PautaId pautaId = PautaId.withUUID(uuid);
        return contabilizarVotos.handle(new ContabilizarVotosCommand(pautaId));
    }

    @PostMapping(value = "/{pautaId}/abrirSessao")
    public Pauta abrirSessao(@PathVariable("pautaId") UUID uuid, @RequestBody AbrirSessaoDTO abrirSessaoDTO) {
        AbrirSessaoCommand command = new AbrirSessaoCommand(PautaId.withUUID(uuid), abrirSessaoDTO.duracao);
        Pauta pautaComSessaoAberta = abrirSessao.handle(command);
        return pautaComSessaoAberta;
    }

    @PostMapping(value = "/{pautaId}/receberVoto")
    public void receberVoto(@PathVariable("pautaId") UUID uuid, @RequestBody VotoDTO votoDTO) {
        PautaId pautaId = PautaId.withUUID(uuid);
        AssociadoId associadoId = AssociadoId.withUUID(votoDTO.associadoId);
        boolean voto = votoDTO.voto;
        ReceberVotoCommand receberVotoCommand = new ReceberVotoCommand(pautaId, associadoId, voto);
        receberVoto.handle(receberVotoCommand);
    }

    private static class VotoDTO {
        @NonNull
        private final UUID associadoId;
        @NonNull
        private final boolean voto;

        @JsonCreator
        public VotoDTO(UUID associadoId, boolean voto) {
            this.associadoId = associadoId;
            this.voto = voto;
        }

        public UUID getAssociadoId() {
            return associadoId;
        }

        public boolean isVoto() {
            return voto;
        }
    }

    public static class AbrirSessaoDTO {

        private final Duration duracao;

        @JsonCreator
        public AbrirSessaoDTO(Duration duracao) {
            this.duracao = duracao == null ? Pauta.DURACAO_PADRAO_DE_SESSAO_DE_VOTACAO : duracao;
        }

        public Duration getDuracao() {
            return duracao;
        }
    }
}
