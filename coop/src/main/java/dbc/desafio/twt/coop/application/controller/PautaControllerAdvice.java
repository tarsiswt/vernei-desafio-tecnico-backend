package dbc.desafio.twt.coop.application.controller;

import dbc.desafio.twt.coop.domain.associado.model.AssociadoNaoEncontradoException;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.voto.application.ServicoDeValidacaoDeCPF.CPFInvalidoException;
import dbc.desafio.twt.coop.domain.voto.application.ServicoDeValidacaoDeCPF.StatusDesconhecidoException;
import dbc.desafio.twt.coop.domain.voto.model.ColetorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.SessaoAindaNaoFoiEncerradaException;
import dbc.desafio.twt.coop.domain.voto.model.SessaoDeVotacaoFechadaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class PautaControllerAdvice {

    @ExceptionHandler({PautaNaoEncontradaException.class, AssociadoNaoEncontradoException.class})
    public ResponseEntity<APIError> excecoesParaEmentosNaoEncontrados(Exception exception) {
        APIError apiError = handle(exception);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiError);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({SessaoAindaNaoFoiEncerradaException.class,
            SessaoDeVotacaoFechadaException.class,
            StatusDesconhecidoException.class,
            CPFInvalidoException.class,
            ColetorDeVotos.VotoInvalidoException.class})
    public ResponseEntity<APIError> excecoesParaValidacoes(Exception exception) {
        APIError apiError = handle(exception);
        return ResponseEntity.badRequest().body(apiError);
    }

    private APIError handle(Exception exception) {
        return new APIError(exception.getMessage());
    }

    private class APIError {
        public final Map<String, Object> propriedades = new HashMap<>();
        public final String mensagem;

        public APIError(String message) {
            this.mensagem = message;
        }
    }
}
