package dbc.desafio.twt.coop.domain.associado.model;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Associado {
    @JsonUnwrapped
    @EmbeddedId
    private AssociadoId id;
    @CPF
    @NotNull
    private String CPF;
    @NotNull
    private String nome;
    private long version;

    protected Associado() {
    }

    protected Associado(String nome, String CPF) {
        this.nome = nome;
        this.CPF = CPF;
        this.id = new AssociadoId(UUID.randomUUID());
    }

    public static Associado novoAssociado(String nome, String CPF) {
        return new Associado(nome, CPF);
    }

    public String getCPF() {
        return CPF;
    }

    public String getNome() {
        return nome;
    }

    public AssociadoId getId() {
        return id;
    }

    protected long getVersion() {
        return version;
    }

    protected void setVersion(long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Associado associado = (Associado) o;
        return getVersion() == associado.getVersion() &&
               Objects.equals(getId(), associado.getId()) &&
               Objects.equals(getCPF(), associado.getCPF()) &&
               Objects.equals(nome, associado.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCPF(), nome, getVersion());
    }

    @Override
    public String toString() {
        return "Associado{" +
               "id=" + id +
               ", CPF='" + CPF + '\'' +
               ", nome='" + nome + '\'' +
               '}';
    }

    @Embeddable
    public static class AssociadoId implements Serializable {

        @GeneratedValue
        @Type(type = "uuid-char")
        private UUID id;

        protected AssociadoId() {
        }

        protected AssociadoId(UUID id) {
            this.id = id;
        }

        public static AssociadoId withUUID(UUID id) {
            return new AssociadoId(id);
        }

        public UUID getId() {
            return id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AssociadoId that = (AssociadoId) o;
            return Objects.equals(id, that.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
