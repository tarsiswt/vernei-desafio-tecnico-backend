package dbc.desafio.twt.coop.domain.associado.model;

import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, AssociadoId> {
    Optional<Associado> findByCPF(String CPF);
}
