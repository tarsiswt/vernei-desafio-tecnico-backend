package dbc.desafio.twt.coop.domain.associado.model;

public class CriarAssociadoCommand {
    private final String nome;
    private final String CPF;

    public CriarAssociadoCommand(String nome, String CPF) {
        this.nome = nome;
        this.CPF = CPF;
    }

    public String getNome() {
        return nome;
    }

    public String getCPF() {
        return CPF;
    }
}
