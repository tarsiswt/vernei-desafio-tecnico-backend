package dbc.desafio.twt.coop.domain.associado.ports;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.associado.model.CriarAssociadoCommand;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
class AssociadoService implements CriarAssociado {

    private final AssociadoRepository associadoRepository;

    public AssociadoService(AssociadoRepository associadoRepository) {
        this.associadoRepository = associadoRepository;
    }

    @Override
    @Transactional
    public Associado handle(CriarAssociadoCommand criarAssociadoCommand) {
        Associado novoAssociado = Associado.novoAssociado(criarAssociadoCommand.getNome(), criarAssociadoCommand.getCPF());
        return associadoRepository.save(novoAssociado);
    }
}
