package dbc.desafio.twt.coop.domain.associado.ports;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.CriarAssociadoCommand;

public interface CriarAssociado {
    Associado handle(CriarAssociadoCommand criarAssociadoCommand);
}
