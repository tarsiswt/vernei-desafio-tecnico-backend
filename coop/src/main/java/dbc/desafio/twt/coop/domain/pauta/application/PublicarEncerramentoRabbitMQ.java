package dbc.desafio.twt.coop.domain.pauta.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.pauta.ports.PublicarEncerramentoEResultadoDeSessao;
import dbc.desafio.twt.coop.domain.voto.model.ContabilizarVotosCommand;
import dbc.desafio.twt.coop.domain.voto.model.SessaoAindaNaoFoiEncerradaException;
import dbc.desafio.twt.coop.domain.voto.ports.ContabilizarVotos;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Map;

@Component
public class PublicarEncerramentoRabbitMQ implements PublicarEncerramentoEResultadoDeSessao {

    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private final ContabilizarVotos contabilizarVotos;
    private final ObjectMapper objectMapper;

    @Autowired
    public PublicarEncerramentoRabbitMQ(RabbitTemplate rabbitTemplate,
                                        @Qualifier("filaDeEncerramentoDeSessao") Queue queue,
                                        ContabilizarVotos contabilizarVotos,
                                        ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
        this.contabilizarVotos = contabilizarVotos;
        this.objectMapper = objectMapper;
    }

    @Override
    @Transactional
    public void publicarEncerramento(Pauta pauta) {
        try {
            MensagemComResultadoDaVotacao mensagem = construirMenagemComOResultadoDaSessao(pauta);
            rabbitTemplate.convertAndSend(queue.getName(), objectMapper.writeValueAsString(mensagem));
        } catch (SessaoAindaNaoFoiEncerradaException | PautaNaoEncontradaException exception) {
            throw new MensagemNaoEnviadaException("Não foi possível contabilizar os votos da pauta " + pauta.getId(),
                    exception);
        } catch (JsonProcessingException | AmqpException exception) {
            throw new MensagemNaoEnviadaException("Não foi possível enviar a mensagem com o resultado " +
                                                  "da votação da pauta " + pauta.getId(), exception);
        }
    }

    private MensagemComResultadoDaVotacao construirMenagemComOResultadoDaSessao(Pauta pauta) {
        Map<Boolean, Long> resultadoDaApuracaoDosVotos = contabilizarVotos.handle(new ContabilizarVotosCommand(pauta.getId()));
        return new MensagemComResultadoDaVotacao(resultadoDaApuracaoDosVotos, pauta.getId());
    }

    public static class MensagemComResultadoDaVotacao {
        private Map<Boolean, Long> contagemDeVotos;
        private Pauta.PautaId pautaId;

        public MensagemComResultadoDaVotacao(Map<Boolean, Long> contagemDeVotos, Pauta.PautaId pautaId) {
            this.contagemDeVotos = contagemDeVotos;
            this.pautaId = pautaId;
        }

        public Map<Boolean, Long> getContagemDeVotos() {
            return contagemDeVotos;
        }

        public void setContagemDeVotos(Map<Boolean, Long> contagemDeVotos) {
            this.contagemDeVotos = contagemDeVotos;
        }

        @JsonUnwrapped
        @JsonProperty("pautaId")
        public Pauta.PautaId getPautaId() {
            return pautaId;
        }

        public void setPautaId(Pauta.PautaId pautaId) {
            this.pautaId = pautaId;
        }
    }

    // TODO: mover para application
    @Configuration
    public static class ConfiguracaoDaFilaDeSessoesEncerradas {
        @Bean
        public Queue filaDeEncerramentoDeSessao(@Value("${app.rabbitmq.queue}") String nomeDaFilaParaSessoesEncerradas) {
            return new Queue(nomeDaFilaParaSessoesEncerradas);
        }
    }

    private class MensagemNaoEnviadaException extends RuntimeException {
        public MensagemNaoEnviadaException(String message) {
            super(message);
        }

        public MensagemNaoEnviadaException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
