package dbc.desafio.twt.coop.domain.pauta.model;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;

import java.time.Duration;
import java.util.Objects;

public class AbrirSessaoCommand {
    private final PautaId pautaId;
    private final Duration duracao;

    public AbrirSessaoCommand(PautaId pautaId, Duration duracao) {
        this.pautaId = pautaId;
        this.duracao = duracao;
    }

    public PautaId getPautaId() {
        return pautaId;
    }

    public Duration getDuracao() {
        return duracao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbrirSessaoCommand that = (AbrirSessaoCommand) o;
        return Objects.equals(pautaId, that.pautaId) &&
               Objects.equals(duracao, that.duracao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pautaId, duracao);
    }

    @Override
    public String toString() {
        return "AbrirSessaoCommand{" +
               "pautaId=" + pautaId +
               ", duracao=" + duracao +
               '}';
    }
}
