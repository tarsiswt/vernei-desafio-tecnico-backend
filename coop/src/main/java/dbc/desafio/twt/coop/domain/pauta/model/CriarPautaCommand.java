package dbc.desafio.twt.coop.domain.pauta.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.lang.NonNull;

public class CriarPautaCommand {
    @NonNull
    private final String descricao;

    @JsonCreator
    public CriarPautaCommand(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
