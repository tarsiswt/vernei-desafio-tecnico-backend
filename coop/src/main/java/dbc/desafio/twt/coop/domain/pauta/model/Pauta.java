package dbc.desafio.twt.coop.domain.pauta.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.common.base.Preconditions;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Pauta {
    public static final Duration DURACAO_PADRAO_DE_SESSAO_DE_VOTACAO = Duration.ofSeconds(60);
    @JsonProperty
    private Sessao sessao;
    @NotNull
    private String descricao;
    @JsonUnwrapped
    @EmbeddedId
    private PautaId id;
    @Version
    private long version;

    protected Pauta() {
    }

    private Pauta(UUID uuid, String descricao) {
        this.id = new PautaId(uuid);
        this.descricao = descricao;
        this.sessao = new Sessao();
    }

    public static Pauta novaPauta(String pauta) {
        return new Pauta(UUID.randomUUID(), pauta);
    }

    protected Long getVersion() {
        return version;
    }

    protected void setVersion(Long version) {
        this.version = version;
    }

    public PautaId getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void abrirSessao() {
        abrirSessao(DURACAO_PADRAO_DE_SESSAO_DE_VOTACAO);
    }

    public void abrirSessao(Duration duracao) {
        sessao.abrirSessao(duracao);
    }

    public void encerrarSessaoManualmente() {
        sessao.encerrarSessao();
    }

    public void marcarResultadoDaSessaoComoPublicada() {
        sessao.encerramentoPublicado();
    }

    public boolean estaAberta() {
        return sessao.estaAberta();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pauta pauta = (Pauta) o;
        return version == pauta.version &&
               Objects.equals(sessao, pauta.sessao) &&
               Objects.equals(getDescricao(), pauta.getDescricao()) &&
               Objects.equals(getId(), pauta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessao, getDescricao(), getId(), version);
    }

    @Override
    public String toString() {
        return "Pauta{" +
               "sessao=" + sessao +
               ", descricao='" + descricao + '\'' +
               ", id=" + id +
               ", version=" + version +
               '}';
    }

    @Embeddable
    public static class PautaId implements Serializable {

        @GeneratedValue
        @Type(type = "uuid-char")
        private UUID id;

        protected PautaId() {
        }

        protected PautaId(UUID id) {
            this.id = id;
        }

        public static PautaId withUUID(UUID id) {
            return new PautaId(id);
        }

        public UUID getId() {
            return id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PautaId pautaId = (PautaId) o;
            return Objects.equals(getId(), pautaId.getId());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getId());
        }

        @Override
        public String toString() {
            return "PautaId{" +
                   "id=" + id +
                   '}';
        }
    }

    @Embeddable
    static class Sessao implements Serializable {

        @JsonProperty
        private Estado estado = Estado.AGUARDANDO_ABERTURA;
        @JsonProperty
        private ZonedDateTime abertaEm = null;
        @JsonProperty
        private Duration duracao = null;
        @JsonProperty
        private ZonedDateTime encerraEm = null;
        @JsonProperty
        private boolean encerramentoPublicado = false;

        protected Sessao() {
        }

        void encerrarSessao() {
            Preconditions.checkState(estado == Estado.ABERTA);
            estado = Estado.FECHADA;
        }

        boolean estaAberta() {
            return estado == Estado.ABERTA && aindaNaoChegouAoFim();
        }

        private boolean aindaNaoChegouAoFim() {
            if (abertaEm != null && duracao != null) {
                ZonedDateTime fimDaSessao = abertaEm.plus(duracao);
                return ZonedDateTime.now().isBefore(fimDaSessao);
            }
            return false;
        }

        void abrirSessao(Duration duracao) {
            Preconditions.checkState(estado == Estado.AGUARDANDO_ABERTURA);
            this.abertaEm = ZonedDateTime.now();
            this.duracao = duracao;
            this.estado = Estado.ABERTA;
            this.encerraEm = abertaEm.plus(duracao);
        }

        void encerramentoPublicado() {
            encerramentoPublicado = true;
        }

        @Override
        public String toString() {
            return "Sessao{" +
                   "estado=" + estado +
                   ", abertaEm=" + abertaEm +
                   ", duracao=" + duracao +
                   ", encerraEm=" + encerraEm +
                   ", encerramentoPublicado=" + encerramentoPublicado +
                   '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Sessao sessao = (Sessao) o;
            return encerramentoPublicado == sessao.encerramentoPublicado &&
                   estado == sessao.estado &&
                   Objects.equals(abertaEm, sessao.abertaEm) &&
                   Objects.equals(duracao, sessao.duracao) &&
                   Objects.equals(encerraEm, sessao.encerraEm);
        }

        @Override
        public int hashCode() {
            return Objects.hash(estado, abertaEm, duracao, encerraEm, encerramentoPublicado);
        }

        private enum Estado {AGUARDANDO_ABERTURA, ABERTA, FECHADA,}
    }
}
