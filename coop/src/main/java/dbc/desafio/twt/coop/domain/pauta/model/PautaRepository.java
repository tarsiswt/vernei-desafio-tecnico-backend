package dbc.desafio.twt.coop.domain.pauta.model;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, PautaId> {
    List<Pauta> findAllByDescricao(String descricao);

    Optional<Pauta> findByDescricao(String descricao);

    @Query("select p from Pauta p where p.sessao.encerraEm <= :dataReferencia and p.sessao.encerramentoPublicado = false")
    List<Pauta> sessoesEncerradasQueAindaNaoForamPublicadas(@Param("dataReferencia") ZonedDateTime dataReferencia);
}
