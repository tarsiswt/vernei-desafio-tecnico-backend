package dbc.desafio.twt.coop.domain.pauta.ports;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.AbrirSessaoCommand;

public interface AbrirSessao {
    Pauta handle(AbrirSessaoCommand command);
}
