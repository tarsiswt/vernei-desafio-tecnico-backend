package dbc.desafio.twt.coop.domain.pauta.ports;

import dbc.desafio.twt.coop.domain.pauta.model.CriarPautaCommand;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;

public interface CriarNovaPauta {
    public Pauta handle(CriarPautaCommand command);
}
