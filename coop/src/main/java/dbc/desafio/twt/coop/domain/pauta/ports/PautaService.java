package dbc.desafio.twt.coop.domain.pauta.ports;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.pauta.model.AbrirSessaoCommand;
import dbc.desafio.twt.coop.domain.pauta.model.CriarPautaCommand;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.Optional;

@Service
class PautaService implements CriarNovaPauta, AbrirSessao {

    private final PautaRepository pautaRepository;
    private final Optional<PublicarEncerramentoEResultadoDeSessao> publicarEncerramento;

    Logger logger = LoggerFactory.getLogger(PautaService.class);

    @Autowired
    public PautaService(PautaRepository pautaRepository, Optional<PublicarEncerramentoEResultadoDeSessao> publicarEncerramento) {
        this.pautaRepository = pautaRepository;
        this.publicarEncerramento = publicarEncerramento;
    }

    @Override
    @Transactional
    public Pauta handle(CriarPautaCommand command) {
        logger.info(command.toString());
        Pauta novaPauta = pautaRepository.save(Pauta.novaPauta(command.getDescricao()));
        logger.info("Pauta {} criada", novaPauta);
        return novaPauta;
    }

    @Override
    @Transactional
    public Pauta handle(AbrirSessaoCommand command) {
        logger.info(command.toString());
        Pauta pauta = pautaRepository.findById(command.getPautaId()).orElseThrow(PautaNaoEncontradaException::new);
        pauta.abrirSessao(command.getDuracao());
        return pauta;

    }

    @Scheduled(initialDelay = 5000, fixedDelay = 5000)
    @Transactional
    public void publicarSessoesEncerradas() {
        logger.info("Iniciando a publicação de pautas encerradas.");
        pautaRepository.sessoesEncerradasQueAindaNaoForamPublicadas(ZonedDateTime.now())
                .stream()
                .peek(pauta -> logger.info("Marcando sessão da pauta {} como publicada.", pauta.getId()))
                .forEach(pauta -> {
                    publicarEncerramento.ifPresent(publicar -> publicar.publicarEncerramento(pauta));
                    pauta.marcarResultadoDaSessaoComoPublicada();
                });
    }
}
