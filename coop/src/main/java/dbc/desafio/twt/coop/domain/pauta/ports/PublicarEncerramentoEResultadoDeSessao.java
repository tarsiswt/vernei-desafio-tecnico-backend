package dbc.desafio.twt.coop.domain.pauta.ports;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;

public interface PublicarEncerramentoEResultadoDeSessao {
    void publicarEncerramento(Pauta pauta);
}
