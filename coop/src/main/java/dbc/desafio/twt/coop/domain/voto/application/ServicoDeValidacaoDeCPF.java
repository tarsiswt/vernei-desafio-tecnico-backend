package dbc.desafio.twt.coop.domain.voto.application;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CharMatcher;
import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.voto.ports.VerificarSeAssociadoPodeVotar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Profile("!test")
@Service
@VisibleForTesting
public class ServicoDeValidacaoDeCPF implements VerificarSeAssociadoPodeVotar {

    public static final String URL_DO_SERVICO_EXTERNO = "https://user-info.herokuapp.com/users";
    private final RestTemplate restTemplate;

    @Autowired
    public ServicoDeValidacaoDeCPF(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri(URL_DO_SERVICO_EXTERNO).build();
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    @Override
    public boolean podeVotar(Associado associado) {
        String CPF = extrairApenasOsNumerosDoCPF(associado.getCPF());
        ResponseEntity<RespostaDTO> resposta = enviarRequisicao(CPF);
        boolean respostaCPFEValido = resposta.getStatusCode() != HttpStatus.NOT_FOUND;
        if (!respostaCPFEValido) {
            throw new CPFInvalidoException("O serviço " + URL_DO_SERVICO_EXTERNO + " informou que o CPF " + CPF + " é inválido.");
        }
        String status = resposta.getBody().status;
        if (status.equals(RespostaDTO.ABLE_TO_VOTE)) {
            return true;
        } else if (status.equals(RespostaDTO.UNABLE_TO_VOTE)) {
            return false;
        } else {
            throw new StatusDesconhecidoException();
        }
    }

    private ResponseEntity<RespostaDTO> enviarRequisicao(String CPF) {
        try {
            return restTemplate.getForEntity("/{cpf}", RespostaDTO.class, CPF);
        } catch (HttpClientErrorException.NotFound notFoundException) {
            return ResponseEntity.notFound().build();
        }
    }

    private String extrairApenasOsNumerosDoCPF(String CPF) {
        return CharMatcher.inRange('0', '9').retainFrom(CPF);
    }

    @VisibleForTesting
    public static class RespostaDTO {

        public static final String ABLE_TO_VOTE = "ABLE_TO_VOTE";
        public static final String UNABLE_TO_VOTE = "UNABLE_TO_VOTE";
        String status;

        @JsonCreator
        public RespostaDTO(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class StatusDesconhecidoException extends RuntimeException {
    }

    public class CPFInvalidoException extends RuntimeException {
        public CPFInvalidoException(String s) {
        }
    }
}
