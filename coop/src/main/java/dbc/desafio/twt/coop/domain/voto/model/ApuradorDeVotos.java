package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ApuradorDeVotos {

    private final PautaRepository pautaRepository;
    private final VotoRepository votoRepository;
    private final Logger logger = LoggerFactory.getLogger(ApuradorDeVotos.class);

    @Autowired
    public ApuradorDeVotos(PautaRepository pautaRepository, VotoRepository votoRepository) {
        this.pautaRepository = pautaRepository;
        this.votoRepository = votoRepository;
    }

    public Map<Boolean, Long> contabilizarVotos(PautaId pautaId) {
        logger.info("Tentando contabilizar votos para {}.", pautaId);
        Pauta pauta = pautaRepository.findById(pautaId).orElseThrow(PautaNaoEncontradaException::new);
        if (pauta.estaAberta()) {
            throw new SessaoAindaNaoFoiEncerradaException();
        } else {
            Map<Boolean, Long> apuracaoDosVotos = votoRepository.findAllById_PautaId(pautaId)
                    .stream()
                    .collect(Collectors.groupingBy(Voto::getVoto, Collectors.counting()));
            preencherComZeroSeNaoHouveVotos(apuracaoDosVotos);
            logger.info("Apuração concluída para a pauta {}.", pautaId);
            return apuracaoDosVotos;
        }
    }

    private void preencherComZeroSeNaoHouveVotos(Map<Boolean, Long> apuracaoDosVotos) {
        apuracaoDosVotos.putIfAbsent(true, 0L);
        apuracaoDosVotos.putIfAbsent(false, 0L);
    }
}
