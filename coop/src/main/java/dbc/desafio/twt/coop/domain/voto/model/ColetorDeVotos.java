package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoNaoEncontradoException;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.voto.ports.VerificarSeAssociadoPodeVotar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class ColetorDeVotos {

    private final PautaRepository pautaRepository;
    private final AssociadoRepository associadoRepository;
    private final VotoRepository votoRepository;
    private final Logger logger = LoggerFactory.getLogger(ApuradorDeVotos.class);
    private final VerificarSeAssociadoPodeVotar verificarSeAssociadoPodeVotar;

    @Autowired
    public ColetorDeVotos(PautaRepository pautaRepository,
                          AssociadoRepository associadoRepository,
                          VotoRepository votoRepository,
                          VerificarSeAssociadoPodeVotar verificarSeAssociadoPodeVotar) {
        this.pautaRepository = pautaRepository;
        this.associadoRepository = associadoRepository;
        this.votoRepository = votoRepository;
        this.verificarSeAssociadoPodeVotar = verificarSeAssociadoPodeVotar;
    }

    public void receberVoto(PautaId pautaId, AssociadoId associadoId, boolean voto) {
        logger.info("Recebida tentativa de voto na pauta {}.", pautaId);
        Associado associado = checkAssociadoExists(associadoId);
        checkAssociadoPodeVotar(associado);
        Pauta pauta = checkPautaExists(pautaId);
        if (pauta.estaAberta()) {
            votoRepository.save(new Voto(pautaId, associadoId, voto));
        } else {
            logger.warn("Tentativa de voto na pauta {} mas a sessão não está aberta. O voto do associado {} não foi contabilizado.",
                    pautaId, associadoId);
            throw new SessaoDeVotacaoFechadaException();
        }
    }

    private void checkAssociadoPodeVotar(Associado associado) {
        try {
            if (!verificarSeAssociadoPodeVotar.podeVotar(associado)) {
                throw new VotoInvalidoException("O associado " + associado + " não pode votar.");
            }
        } catch (RuntimeException ex) {
            throw new VotoInvalidoException(ex);
        }
    }

    public Optional<Voto> getVotoDoAssociadoNaPauta(AssociadoId associadoId, PautaId pautaId) {
        logger.info("Verificando o voto do associado {} na pauta {}.", associadoId, pautaId);
        checkPautaExists(pautaId);
        checkAssociadoExists(associadoId);
        return votoRepository.findById(new Voto.VotoId(pautaId, associadoId));
    }

    public Set<Voto> getTodosOsVotosNaPauta(PautaId pautaId) {
        Pauta pauta = checkPautaExists(pautaId);
        if (pauta.estaAberta()) {
            logger.warn("Coletando os votos da pauta {} mas a sessão ainda está aberta.");
        }
        return votoRepository.findAllById_PautaId(pautaId);
    }

    private Associado checkAssociadoExists(AssociadoId associadoId) {
        return associadoRepository.findById(associadoId).orElseThrow(AssociadoNaoEncontradoException::new);
    }

    private Pauta checkPautaExists(PautaId pautaId) {
        return pautaRepository.findById(pautaId).orElseThrow(PautaNaoEncontradaException::new);
    }

    public class VotoInvalidoException extends RuntimeException {

        public VotoInvalidoException(RuntimeException cause) {
            super(cause);
        }

        public VotoInvalidoException(String message) {
            super(message);
        }
    }
}
