package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;

public class ContabilizarVotosCommand {

    private final Pauta.PautaId pautaId;

    public ContabilizarVotosCommand(Pauta.PautaId pautaId) {
        this.pautaId = pautaId;
    }

    public Pauta.PautaId getPautaId() {
        return pautaId;
    }
}
