package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;

public class ReceberVotoCommand {
    private final PautaId pautaId;
    private final AssociadoId associadoId;
    private final boolean voto;

    public ReceberVotoCommand(PautaId pautaId, AssociadoId associadoId, boolean voto) {
        this.pautaId = pautaId;
        this.associadoId = associadoId;
        this.voto = voto;
    }

    public PautaId getPautaId() {
        return pautaId;
    }

    public AssociadoId getAssociadoId() {
        return associadoId;
    }

    public boolean getVoto() {
        return voto;
    }
}
