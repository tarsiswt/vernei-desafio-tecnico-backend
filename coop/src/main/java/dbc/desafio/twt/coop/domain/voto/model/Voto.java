package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Voto {

    @EmbeddedId
    private VotoId id;

    private boolean voto;

    protected Voto(PautaId pautaId, AssociadoId associadoId, boolean voto) {
        this.id = new VotoId(pautaId, associadoId);
        this.voto = voto;
    }

    protected Voto() {
    }

    public VotoId getId() {
        return id;
    }

    public boolean getVoto() {
        return voto;
    }

    @Embeddable
    public static class VotoId implements Serializable {

        @Embedded
        @AttributeOverrides(@AttributeOverride(name = "id", column = @Column(name = "pauta_id")))
        PautaId pautaId;
        @Embedded
        @AttributeOverrides(@AttributeOverride(name = "id", column = @Column(name = "associado_id")))
        AssociadoId associadoId;

        public VotoId(PautaId pautaId, AssociadoId associadoId) {
            this.pautaId = pautaId;
            this.associadoId = associadoId;
        }

        public VotoId() {
        }

        public PautaId getPautaId() {
            return pautaId;
        }

        public AssociadoId getAssociadoId() {
            return associadoId;
        }
    }
}
