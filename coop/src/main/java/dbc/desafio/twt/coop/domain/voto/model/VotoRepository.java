package dbc.desafio.twt.coop.domain.voto.model;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.voto.model.Voto.VotoId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
interface VotoRepository extends JpaRepository<Voto, VotoId> {
    Set<Voto> findAllById_PautaId(PautaId pautaId);
}
