package dbc.desafio.twt.coop.domain.voto.ports;

import dbc.desafio.twt.coop.domain.voto.model.ContabilizarVotosCommand;

import java.util.Map;

public interface ContabilizarVotos {
    Map<Boolean, Long> handle(ContabilizarVotosCommand command);
}
