package dbc.desafio.twt.coop.domain.voto.ports;

import dbc.desafio.twt.coop.domain.voto.model.ReceberVotoCommand;

public interface ReceberVoto {
    void handle(ReceberVotoCommand command);
}
