package dbc.desafio.twt.coop.domain.voto.ports;

import dbc.desafio.twt.coop.domain.associado.model.Associado;

public interface VerificarSeAssociadoPodeVotar {
    boolean podeVotar(Associado associado);
}
