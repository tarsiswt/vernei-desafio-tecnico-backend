package dbc.desafio.twt.coop.domain.voto.ports;

import dbc.desafio.twt.coop.domain.voto.model.ApuradorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.ColetorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.ContabilizarVotosCommand;
import dbc.desafio.twt.coop.domain.voto.model.ReceberVotoCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
class VotoService implements ContabilizarVotos, ReceberVoto {

    private final ApuradorDeVotos apuradorDeVotos;
    private final ColetorDeVotos coletorDeVotos;

    @Autowired
    public VotoService(ApuradorDeVotos apuradorDeVotos, ColetorDeVotos coletorDeVotos) {
        this.apuradorDeVotos = apuradorDeVotos;
        this.coletorDeVotos = coletorDeVotos;
    }

    @Override
    public Map<Boolean, Long> handle(ContabilizarVotosCommand command) {
        return apuradorDeVotos.contabilizarVotos(command.getPautaId());
    }

    @Override
    public void handle(ReceberVotoCommand command) {
        coletorDeVotos.receberVoto(command.getPautaId(), command.getAssociadoId(), command.getVoto());
    }
}
