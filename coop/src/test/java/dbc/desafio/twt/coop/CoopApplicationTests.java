package dbc.desafio.twt.coop;

import dbc.desafio.twt.coop.domain.voto.ports.ServicoQueSempreValidaCPFParaTestes;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@SpringBootTest
@Import(ServicoQueSempreValidaCPFParaTestes.class)
class CoopApplicationTests {

    @Test
    @Ignore
    void contextoNaoCarregaQuandoUmaImplementacaoParaValidarSeAssociadoPodeVotar() {
        // TODO: encontrar uma maneira de testar que o contexto *falha* no carregamento quando um determinado bean não é encontrado
    }

}
