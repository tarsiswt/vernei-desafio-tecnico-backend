package dbc.desafio.twt.coop.application.controller;

import dbc.desafio.twt.coop.application.controller.PautaController.AbrirSessaoDTO;
import dbc.desafio.twt.coop.domain.pauta.model.CriarPautaCommand;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.voto.ports.ServicoQueSempreValidaCPFParaTestes;
import org.assertj.core.api.BDDAssumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.*;

import java.time.Duration;
import java.util.List;
import java.util.UUID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(ServicoQueSempreValidaCPFParaTestes.class)
class PautaControllerTest {

    public static final String DESCRICAO_DA_PAUTA = "Descrição da pauta";
    @Autowired
    PautaController pautaController;
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    @DisplayName("POST em /pauta seguido de POST em /pauta/{id}/abrirSessao abre a sessão da pauta recém-criada")
    public void criarPautaEAbrirASessao() {
        CriarPautaCommand criarPautaCommand = new CriarPautaCommand(DESCRICAO_DA_PAUTA);
        HttpEntity<CriarPautaCommand> requisicaoCriarPauta = httpEntityBasicoParaV1DaApi(criarPautaCommand);
        var pautaCriada = restTemplate.exchange("/pauta",
                HttpMethod.POST,
                requisicaoCriarPauta,
                Pauta.class)
                .getBody();

        AbrirSessaoDTO abrirSessaoDTO = new AbrirSessaoDTO(Duration.ofSeconds(50));
        HttpEntity<AbrirSessaoDTO> requisicaoAbrirSessao = httpEntityBasicoParaV1DaApi(abrirSessaoDTO);

        var respostaDaAberturaDaSessao = restTemplate.exchange("/pauta/{id}/abrirSessao",
                HttpMethod.POST,
                requisicaoAbrirSessao,
                Pauta.class,
                pautaCriada.getId().getId());

        assert respostaDaAberturaDaSessao.getStatusCode() == HttpStatus.OK;

        Pauta pautaComSessaoAberta = respostaDaAberturaDaSessao.getBody();
        assert pautaCriada.getId().equals(pautaComSessaoAberta.getId());
    }

    @Test
    @DisplayName("POST em /pauta enviando um CriaPautaCommand resulta na criação de uma nova pauta")
    public void criarNovaPautaResultaEmNovaPauta() {
        CriarPautaCommand criarPautaCommand = new CriarPautaCommand(DESCRICAO_DA_PAUTA);
        HttpEntity<CriarPautaCommand> request = httpEntityBasicoParaV1DaApi(criarPautaCommand);

        var response = restTemplate.exchange("/pauta",
                HttpMethod.POST,
                request,
                Pauta.class);

        assert response.getStatusCode() == HttpStatus.OK;
        Pauta pautaCriada = response.getBody();
        assert pautaCriada != null;
        assert pautaCriada.getId() != null;
        assert pautaCriada.getDescricao().equals(DESCRICAO_DA_PAUTA);
    }

    @Test
    @DisplayName("GET em /pauta/{id}/contabilizarVotos com um id aleatório resulta em 404 Not Found.")
    public void contabilizarVotosEmUmaPautaAleatoriaResultaEm404NotFound() {
        PautaId pautaId = PautaId.withUUID(UUID.randomUUID());

        HttpEntity<String> request = httpEntityBasicoParaV1DaApi("");
        var response = restTemplate.exchange("/pauta/{id}/contabilizarVotos",
                HttpMethod.GET,
                request,
                Pauta.class,
                pautaId.getId());

        assert response.getStatusCode() == HttpStatus.NOT_FOUND;
    }

    @Test
    @DisplayName("GET em /pauta/{id}/contabilizarVotos com Accept: vnd.dbc.coop.api-v2+json resultam em 406 Not Acceptable.")
    public void getPautaComVersaoIncorretaDaAPIResultaEm406() {
        PautaId pautaId = PautaId.withUUID(UUID.randomUUID());

        HttpEntity<String> request = httpEntityBasico("application", "vnd.dbc.coop.api-v2+json", "");
        ResponseEntity<Pauta> response = restTemplate.exchange("/pauta/{id}/contabilizarVotos",
                HttpMethod.GET,
                request,
                Pauta.class,
                pautaId.getId());

        assert response.getStatusCode() == HttpStatus.NOT_ACCEPTABLE;
    }

    private <T> HttpEntity<T> httpEntityBasicoParaV1DaApi(T body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(new MediaType("application", "vnd.dbc.coop.api-v1+json")));
        return new HttpEntity<>(body, headers);
    }

    private <T> HttpEntity<T> httpEntityBasico(String mediaType, String subType, T body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(new MediaType(mediaType, subType)));
        return new HttpEntity<>(body, headers);
    }
}
