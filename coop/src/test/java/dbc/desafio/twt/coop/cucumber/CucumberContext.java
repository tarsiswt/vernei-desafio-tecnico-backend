package dbc.desafio.twt.coop.cucumber;

import dbc.desafio.twt.coop.domain.voto.ports.ServicoQueSempreValidaCPFParaTestes;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@CucumberContextConfiguration
@SpringBootTest()
@Import(ServicoQueSempreValidaCPFParaTestes.class)
public class CucumberContext {
}

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources")
@Configuration
class CucumberConfigurations {
}
