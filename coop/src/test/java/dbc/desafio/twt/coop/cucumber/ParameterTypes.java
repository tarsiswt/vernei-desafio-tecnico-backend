package dbc.desafio.twt.coop.cucumber;

import io.cucumber.java.ParameterType;

public class ParameterTypes {

    @ParameterType("\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}")
    public String CPF(String CPF) {
        return CPF;
    }

    // @formatter:off
    @ParameterType("[Ss]im|[Nn]ão")
    public boolean voto(String voto) {
        switch (voto.toLowerCase()) {
            case "sim": return true;
            case "não": return false;
            default: throw new RuntimeException("Voto inválido: " + voto);
        }
    }

    @ParameterType("[Aa]berta|[Ff]echada")
    public Boolean estadoDaSessao(String estado) {
        switch (estado) {
            case "aberta": return true;
            case "fechada": return false;
            default: throw new RuntimeException("Estado inválido para a sessão: " + estado);
        }
    }
    // @formatter:on
}
