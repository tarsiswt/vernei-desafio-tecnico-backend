package dbc.desafio.twt.coop.cucumber.steps;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import io.cucumber.java.pt.Dados;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public class AssociadoSteps {
    @Autowired
    AssociadoRepository associadoRepository;

    @Dados("os associados")
    public void osAssociados(List<Map<String, String>> tabelaDeAssociadosNoCenario) {
        tabelaDeAssociadosNoCenario.stream()
                .map(nomeECPF -> Associado.novoAssociado(nomeECPF.get("Nome"), nomeECPF.get("CPF do Associado")))
                .forEach(associadoRepository::save);
    }
}
