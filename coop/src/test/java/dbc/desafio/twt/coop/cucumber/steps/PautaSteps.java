package dbc.desafio.twt.coop.cucumber.steps;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.pauta.model.CriarPautaCommand;
import dbc.desafio.twt.coop.domain.pauta.ports.CriarNovaPauta;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class PautaSteps {

    @Autowired
    private PautaRepository pautaRepository;
    @Autowired
    private AssociadoRepository associadoRepository;
    @Autowired
    private CriarNovaPauta criarNovaPauta;

    @Dado("(que )uma nova pauta {string} é cadastrada")
    public void umaNovaPautaÉCadastrada(String pautaDoCenario) {
        criarNovaPauta.handle(new CriarPautaCommand(pautaDoCenario));
    }

    @E("a pauta recebe uma identificação única")
    public void aPautaRecebeUmaIdentificaçãoÚnica() {
        List<Pauta> todasAsPautas = pautaRepository.findAll();
        var todosOsIdentificadoresDasPautas = todasAsPautas.stream()
                .map(Pauta::getId)
                .collect(Collectors.toSet());
        assert todasAsPautas.size() == todosOsIdentificadoresDasPautas.size();
    }

    @Então("as pautas {string} possuem identificações distintas")
    public void asPautasPossuemIdentificaçõesDistintas(String pautaDoCenario) {
        List<Pauta> todasAsPautasComADescricaoDoCenario = pautaRepository.findAllByDescricao(pautaDoCenario);
        var identificadoresDasPautas = todasAsPautasComADescricaoDoCenario.stream()
                .map(Pauta::getId)
                .collect(Collectors.toSet());
        assert todasAsPautasComADescricaoDoCenario.size() == identificadoresDasPautas.size();
    }

    @E("a/uma sessão da pauta {string} está {estadoDaSessao}")
    public void aSessãoDaPautaEstáAbertaOuFechada(String pautaDoCenario, boolean sessaoEstaAbertaNoCenario) {
        Pauta pauta = getPautaOrFailIfNotFound(pautaDoCenario);
        assert pauta.estaAberta() == sessaoEstaAbertaNoCenario;
    }

    // TODO: refatorar e mover para para outra classe
    @E("o/a/um/uma associado/associada {string} com CPF {CPF}")
    public void oAssociadoComCPF(String nomeDoAssociadoNoCenario, String CPFDoAssociadoNoCenario) {
        Associado entity = Associado.novoAssociado(nomeDoAssociadoNoCenario, CPFDoAssociadoNoCenario);
        Associado save = associadoRepository.save(entity);
        System.out.println(save);
    }

    @Quando("a sessão da pauta {string} for aberta")
    public void aSessãoDaPautaForAberta(String pautaNoCenario) {
        Pauta pauta = getPautaOrFailIfNotFound(pautaNoCenario);
        pauta.abrirSessao();
    }

    @E("a sessão da pauta {string} for encerrada/fechada")
    public void aSessãoDaPautaForEncerrada(String pautaNoCenario) {
        Pauta pauta = getPautaOrFailIfNotFound(pautaNoCenario);
        pauta.encerrarSessaoManualmente();
    }

    private Pauta getPautaOrFailIfNotFound(String pautaNoCenario) {
        return pautaRepository.findByDescricao(pautaNoCenario).orElseThrow(RuntimeException::new);
    }
}
