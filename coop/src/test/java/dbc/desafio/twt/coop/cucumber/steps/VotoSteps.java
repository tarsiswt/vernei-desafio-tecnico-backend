package dbc.desafio.twt.coop.cucumber.steps;

import dbc.desafio.twt.coop.cucumber.ParameterTypes;
import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.Associado.AssociadoId;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoNaoEncontradoException;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta.PautaId;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.voto.model.ColetorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.Voto;
import dbc.desafio.twt.coop.domain.voto.model.ContabilizarVotosCommand;
import dbc.desafio.twt.coop.domain.voto.model.ReceberVotoCommand;
import dbc.desafio.twt.coop.domain.voto.model.SessaoAindaNaoFoiEncerradaException;
import dbc.desafio.twt.coop.domain.voto.model.SessaoDeVotacaoFechadaException;
import dbc.desafio.twt.coop.domain.voto.ports.ContabilizarVotos;
import dbc.desafio.twt.coop.domain.voto.ports.ReceberVoto;
import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class VotoSteps {

    @Autowired
    private PautaRepository pautaRepository;
    @Autowired
    private AssociadoRepository associadoRepository;
    @Autowired
    private ColetorDeVotos coletorDeVotos;
    @Autowired
    private ReceberVoto receberVoto;
    @Autowired
    private ContabilizarVotos contabilizarVotos;

    @Autowired
    private ParameterTypes parameterTypes;

    private Logger logger = LoggerFactory.getLogger(VotoSteps.class);

    @Quando("o/a/um/uma associado/associada com CPF {CPF} lançar seu voto \"{voto}\" na pauta {string}")
    public void umAssociadoLançaSeuVotoNaPauta(String CPFDoAssociadoNoCenario, boolean votoNoCenario, String pautaNoCenario) {
        var pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        var associadoId = getAssociadoOrFailIfNotFound(CPFDoAssociadoNoCenario).getId();
        try {
            receberVoto.handle(new ReceberVotoCommand(pautaId, associadoId, votoNoCenario));
        } catch (SessaoDeVotacaoFechadaException e) {
            logger.warn(e, () -> "Não foi possível registrar o voto.");
        }
    }

    @Então("o voto do associado com CPF {CPF} na pauta {string} não é contabilizado")
    public void oVotoDoAssociadoComCPFNãoÉContabilizado(String CPFDoAssociadoNoCenario, String pautaNoCenario) {
        AssociadoId associadoId = getAssociadoOrFailIfNotFound(CPFDoAssociadoNoCenario).getId();
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        Optional<Voto> votoDoAssociadoNaPauta = coletorDeVotos.getVotoDoAssociadoNaPauta(associadoId, pautaId);
        assert votoDoAssociadoNaPauta.isEmpty();
    }

    @Então("o voto do associado com CPF {CPF} na pauta {string} é contabilizado")
    public void oVotoDoAssociadoComCPFNaPautaÉContabilizado(String CPFDoAssociadoNoCenario, String pautaNoCenario) {
        AssociadoId associadoId = getAssociadoOrFailIfNotFound(CPFDoAssociadoNoCenario).getId();
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        Optional<Voto> votoDoAssociadoNaPauta = coletorDeVotos.getVotoDoAssociadoNaPauta(associadoId, pautaId);
        assert votoDoAssociadoNaPauta.isPresent();
    }

    @Então("apenas um voto do associado com CPF {CPF} na pauta {string} é contabilizado")
    public void apenasUmVotoDoAssociadoComCPFNaPautaÉContabilizado(String CPFDoAssociadoNoCenario, String pautaNoCenario) {
        AssociadoId associadoId = getAssociadoOrFailIfNotFound(CPFDoAssociadoNoCenario).getId();
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        Set<Voto> todosOsVotosNaPauta = coletorDeVotos.getTodosOsVotosNaPauta(pautaId);
        assert todosOsVotosNaPauta.stream()
                       .filter(voto -> voto.getId().getAssociadoId().equals(associadoId))
                       .count() == 1;
    }

    private Pauta getPautaOrFailIfNotFound(String pautaNoCenario) {
        return pautaRepository.findByDescricao(pautaNoCenario).orElseThrow(RuntimeException::new);
    }

    private Associado getAssociadoOrFailIfNotFound(String CPFDoAssociado) {
        return associadoRepository.findByCPF(CPFDoAssociado).orElseThrow(RuntimeException::new);
    }

    @Então("não é possível contabilizar os votos da pauta {string}")
    public void nãoÉPossívelContabilizarOsVotosDaPauta(String pautaNoCenario) {
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        ContabilizarVotosCommand command = new ContabilizarVotosCommand(pautaId);
        Assertions.assertThrows(SessaoAindaNaoFoiEncerradaException.class,
                () -> contabilizarVotos.handle(command));
    }

    @Então("o total de votos favoráveis da pauta {string} é {long}")
    public void oTotalDeVotosFavoráveisÉFavoraveis(String pautaNoCenario, long quantidadeDeVotosNoCenario) {
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        ContabilizarVotosCommand command = new ContabilizarVotosCommand(pautaId);
        Map<Boolean, Long> apuracaoDosVotos = contabilizarVotos.handle(command);
        assert apuracaoDosVotos.get(true) == quantidadeDeVotosNoCenario;
    }

    @E("o total de votos desfavoráveis da pauta {string} é {long}")
    public void oTotalDeVotosDesfavoráveisDaPautaÉQuantidadeDeVotosDesfavoráveis(String pautaNoCenario,
                                                                                 long quantidadeDeVotosNoCenario) {
        PautaId pautaId = getPautaOrFailIfNotFound(pautaNoCenario).getId();
        ContabilizarVotosCommand command = new ContabilizarVotosCommand(pautaId);
        Map<Boolean, Long> apuracaoDosVotos = contabilizarVotos.handle(command);
        assert apuracaoDosVotos.get(false) == quantidadeDeVotosNoCenario;
    }

    @Quando("os votos são lançados na pauta {string}")
    public void osVotosSãoLançados(String pautaNoCenario, List<Map<String, String>> tabelaDeVotosNoCenario) {
        PautaId pauta = getPautaOrFailIfNotFound(pautaNoCenario).getId();

        for (Map<String, String> votoDoAssociado : tabelaDeVotosNoCenario) {
            boolean voto = parameterTypes.voto(votoDoAssociado.get("Voto"));
            String CPF = votoDoAssociado.get("CPF do Associado");
            AssociadoId associado = associadoRepository.findByCPF(CPF).orElseThrow(AssociadoNaoEncontradoException::new).getId();
            ReceberVotoCommand receberVotoCommand = new ReceberVotoCommand(pauta, associado, voto);

            receberVoto.handle(receberVotoCommand);
        }
    }
}
