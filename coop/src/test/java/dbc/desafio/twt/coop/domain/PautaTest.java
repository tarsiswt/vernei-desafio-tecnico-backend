package dbc.desafio.twt.coop.domain;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;

class PautaTest {

    public static final String DESCRICAO_DA_PAUTA = "Descrição da pauta";

    @Test
    @DisplayName("A sessão de votação de uma pauta recém-criada não está aberta")
    public void sessaoDeVotacaoRecemCriadaNaoEstaAberta() {
        assert !Pauta.novaPauta(DESCRICAO_DA_PAUTA).estaAberta();
    }

    @Test
    @DisplayName("Abrir a sessão de votação de uma pauta torna a sessão aberta")
    public void abrirSessaoDeVotacaoDeUmaPautaTornaASessaoAberta() {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        pauta.abrirSessao();
        assert pauta.estaAberta();

    }

    @Test
    @DisplayName("Abrir a sessão de votação e depois encerrá-la torna a sessão fechada")
    public void abrirSessaoDeVotacaoEEncerrarTornarASessaoFechada() {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        pauta.abrirSessao();
        pauta.encerrarSessaoManualmente();
        assert !pauta.estaAberta();
    }

    @Test
    @DisplayName("Encerrar uma sessão de votação que não foi aberta resulta em IllegalStateException e não altera seu estado")
    public void encerrarSessaoQueNaofoiAbertaResultaEmIllegalStateException() {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        boolean antes = pauta.estaAberta();
        Assertions.assertThrows(IllegalStateException.class, pauta::encerrarSessaoManualmente);
        boolean depois = pauta.estaAberta();
        assert antes == depois;
    }

    @Test
    @DisplayName("Abrir uma sessão de votação já aberta resulta em IllegalStateException e não altera seu estado")
    public void abrirSessaoJaAbertaResultaEmIllegalStateException() {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        pauta.abrirSessao();
        boolean antes = pauta.estaAberta();
        Assertions.assertThrows(IllegalStateException.class, pauta::abrirSessao);
        boolean depois = pauta.estaAberta();
        assert antes == depois;
    }

    @Test
    @DisplayName("A sessão de votação com duração de 100ns é fechada após 100ns")
    public void sessaoDeVotacaoComDuracao100nsEFechadaApos100ns() throws InterruptedException {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        pauta.abrirSessao(Duration.ofNanos(100));
        Thread.sleep(0, 100);
        assert !pauta.estaAberta();
    }

    @Test
    @DisplayName("A sessão de votação com duração de 500ms ainda está aberta após 250ms")
    public void sessaoDeVotacaoComDuracao500msAindaEstaAbertaApos250ms() throws InterruptedException {
        Pauta pauta = Pauta.novaPauta(DESCRICAO_DA_PAUTA);
        pauta.abrirSessao(Duration.ofMillis(500));
        Thread.sleep(250);
        assert pauta.estaAberta();
    }
}
