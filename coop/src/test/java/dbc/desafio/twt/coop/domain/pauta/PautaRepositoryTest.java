package dbc.desafio.twt.coop.domain.pauta;

import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.Duration;
import java.util.List;

import static java.time.ZonedDateTime.now;

@DataJpaTest
class PautaRepositoryTest {

    public static final int DEZ_MILISEGUNDOS = 10;
    public static final Duration DURACAO_MUITO_CURTA = Duration.ofMillis(DEZ_MILISEGUNDOS);
    @Autowired
    PautaRepository pautaRepository;

    @Test
    @DisplayName("Uma pauta cuja sessão acabou de encerrar e que já teve seu resultado publicado " +
                 "não é retornada na consulta por sessões encerradas.")
    void pautaCujaSessaoEncerradaEPublicadaNaoERetornadaNaConsultaPorSessoesEncerradasENaoPublicadas() throws InterruptedException {
        Pauta pautaComSessaoPrestesAEncerrar = Pauta.novaPauta("Uma pauta");
        pautaComSessaoPrestesAEncerrar.abrirSessao(DURACAO_MUITO_CURTA);
        pautaRepository.save(pautaComSessaoPrestesAEncerrar);
        Thread.sleep(DEZ_MILISEGUNDOS);
        pautaComSessaoPrestesAEncerrar.marcarResultadoDaSessaoComoPublicada();
        pautaRepository.save(pautaComSessaoPrestesAEncerrar);
        List<Pauta> pautas = pautaRepository.sessoesEncerradasQueAindaNaoForamPublicadas(now());
        assert pautas.isEmpty();
    }

    @Test
    @DisplayName("Uma pauta cuja sessão acabou de encerrar é retornada na consulta por sessões " +
                 "encerradas e não publicadas.")
    void pautaCujaSessaoEncerradaERetornadaNaConsultaPorSessoesEncerradas() throws InterruptedException {
        Pauta pautaComSessaoPrestesAEncerrar = Pauta.novaPauta("Uma pauta");
        pautaComSessaoPrestesAEncerrar.abrirSessao(DURACAO_MUITO_CURTA);
        pautaRepository.save(pautaComSessaoPrestesAEncerrar);
        Thread.sleep(DEZ_MILISEGUNDOS);

        List<Pauta> pautas = pautaRepository.sessoesEncerradasQueAindaNaoForamPublicadas(now());
        assert pautas.size() == 1;
    }
}
