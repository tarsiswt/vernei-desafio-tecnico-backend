package dbc.desafio.twt.coop.domain.voto;

import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.voto.model.ApuradorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.ColetorDeVotos;
import dbc.desafio.twt.coop.domain.voto.model.SessaoAindaNaoFoiEncerradaException;
import dbc.desafio.twt.coop.domain.voto.ports.ServicoQueSempreValidaCPFParaTestes;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.util.Map;
import java.util.UUID;

@SpringBootTest
@Import(ServicoQueSempreValidaCPFParaTestes.class)
@AutoConfigureMockMvc
class ApuradorDeVotosTest {

    @Autowired
    ApuradorDeVotos apuradorDeVotos;
    @Autowired
    PautaRepository pautaRepository;
    @Autowired
    ColetorDeVotos coletorDeVotos;
    @Autowired
    AssociadoRepository associadoRepository;

    @Test
    @DisplayName("Contabilizar votos em um pauta inexistente resulta em PautaNaoEncontradaException")
    public void contabilizarVotos() {
        Pauta.PautaId pautaIdAleatória = Pauta.PautaId.withUUID(UUID.randomUUID());
        Assertions.assertThatThrownBy(() -> apuradorDeVotos.contabilizarVotos(pautaIdAleatória))
                .isInstanceOf(PautaNaoEncontradaException.class);
    }

    @Test
    @DisplayName("Contabilizar votos em uma pauta cuja sessão ainda não foi encerrada resulta em SessaoAindaNaoFoiEncerradaException")
    public void contabilizarVotosEmPautaCujaSessaoAindaNaoFoiEncerradaResultaEmExcecao() {
        Pauta pauta = Pauta.novaPauta("Descrição da pauta");
        pauta.abrirSessao();
        Pauta pautaComSessaoAberta = pautaRepository.save(pauta);
        Assertions.assertThat(pautaComSessaoAberta.estaAberta()).isTrue();
        Assertions.assertThatThrownBy(() -> apuradorDeVotos.contabilizarVotos(pautaComSessaoAberta.getId()))
                .isInstanceOf(SessaoAindaNaoFoiEncerradaException.class);
    }

    @Test
    @DisplayName("Contabilizar votos em uma pauta cuja sessão não teve votos")
    public void contabilizarVotosEmUmaPautaCujaSessaoNaoTeveVotos() {
        Pauta pauta = Pauta.novaPauta("Descrição da pauta");
        pauta.abrirSessao();
        pauta = pautaRepository.save(pauta);
        pauta.encerrarSessaoManualmente();
        pauta = pautaRepository.save(pauta);

        Map<Boolean, Long> resultadoDaApuracao = apuradorDeVotos.contabilizarVotos(pauta.getId());

        Assertions.assertThat(resultadoDaApuracao.get(true)).isZero();
        Assertions.assertThat(resultadoDaApuracao.get(false)).isZero();
    }
}
