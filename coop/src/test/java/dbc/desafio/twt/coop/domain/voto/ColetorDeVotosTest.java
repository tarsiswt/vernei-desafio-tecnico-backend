package dbc.desafio.twt.coop.domain.voto;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoRepository;
import dbc.desafio.twt.coop.domain.associado.model.AssociadoNaoEncontradoException;
import dbc.desafio.twt.coop.domain.pauta.model.Pauta;
import dbc.desafio.twt.coop.domain.pauta.model.PautaRepository;
import dbc.desafio.twt.coop.domain.pauta.model.PautaNaoEncontradaException;
import dbc.desafio.twt.coop.domain.voto.model.ColetorDeVotos;
import dbc.desafio.twt.coop.domain.voto.ports.ServicoQueSempreValidaCPFParaTestes;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import java.util.UUID;

@SpringBootTest
@Import(ServicoQueSempreValidaCPFParaTestes.class)
class ColetorDeVotosTest {

    @Autowired
    ColetorDeVotos coletorDeVotos;
    @Autowired
    PautaRepository pautaRepository;
    @Autowired
    AssociadoRepository associadoRepository;

    @Test
    @DisplayName("Consultar votos em uma pauta inexistente resulta em PautaNaoEncontradaException")
    void consultarVotosEmPautaInexistenteResultaEmExcecao() {
        Assertions.assertThatThrownBy(() -> coletorDeVotos.getTodosOsVotosNaPauta(pautaIdAleatória()))
                .isInstanceOf(PautaNaoEncontradaException.class);
    }

    @Test
    @DisplayName("Consultar votos de pauta e associado existentes resulta em PautaNaoEncontradaException ou AssociadoNaoEncontradoException")
    void consultarVotosDePautaEAssociadoInexistentesResultaEmExcecao() {
        Assertions.assertThatThrownBy(() -> coletorDeVotos.getVotoDoAssociadoNaPauta(associadoIdAleatório(), pautaIdAleatória()))
                .isInstanceOfAny(PautaNaoEncontradaException.class, AssociadoNaoEncontradoException.class);
    }

    @Test
    @DisplayName("Receber voto de associado e pauta inexistentes resulta em PautaNaoEncontradaException ou AssociadoNaoEncontradoException")
    void receberVotoDeAssociadoEPautaInexistentesResultaEmExcecao() {
        Assertions.assertThatThrownBy(() -> coletorDeVotos.receberVoto(pautaIdAleatória(), associadoIdAleatório(), true))
                .isInstanceOfAny(PautaNaoEncontradaException.class, AssociadoNaoEncontradoException.class);
    }

    @Test
    @DisplayName("Receber voto de associado inexistente em uma pauta resulta em AssociadoNaoEncontradoException")
    void receberVotoDeAssociadoInexistenteEmPautaResultaEmExcecao() {
        Pauta pautaSalva = pautaRepository.save(Pauta.novaPauta("Descrição da pauta"));
        Assertions.assertThatThrownBy(() -> coletorDeVotos.receberVoto(pautaSalva.getId(), associadoIdAleatório(), true))
                .isInstanceOf(AssociadoNaoEncontradoException.class);
    }

    @Test
    @DisplayName("Receber voto de associado em uma pauta inexistente resulta em PautaNaoEncontradaException")
    void receberVotoDeAssociadoEmPautaInexistenteResultaEmExcecao() {
        Associado associadoSalvo = associadoRepository.save(Associado.novoAssociado("Nome do Associado", "066.464.834-77"));
        Assertions.assertThatThrownBy(() -> coletorDeVotos.receberVoto(pautaIdAleatória(), associadoSalvo.getId(), true))
                .isInstanceOf(PautaNaoEncontradaException.class);
    }

    private Pauta.PautaId pautaIdAleatória() {
        return Pauta.PautaId.withUUID(UUID.randomUUID());
    }

    private Associado.AssociadoId associadoIdAleatório() {
        return Associado.AssociadoId.withUUID(UUID.randomUUID());
    }
}
