package dbc.desafio.twt.coop.domain.voto.ports;

import dbc.desafio.twt.coop.domain.associado.model.Associado;
import dbc.desafio.twt.coop.domain.voto.application.ServicoDeValidacaoDeCPF;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.MockServerRestTemplateCustomizer;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
class ServicoDeValidacaoDeCPFTest {

    @Autowired
    ServicoDeValidacaoDeCPF servicoDeValidacaoDeCPF;

    @Test
    @DisplayName("Quando o serviço externo retorna 404 Not Found, o serviço de validação de CPF lança CPFInvalidoException")
    public void quandoServicoExternoRetorna404CPFInvalidoExceptionELancada() {
        MockRestServiceServer server = getMockRestServiceServer();
        server.expect(requestTo("/06646483477")).andRespond(withStatus(HttpStatus.NOT_FOUND));

        Associado associado = Associado.novoAssociado("Nome", "066.464.834-77");

        Assertions.assertThrows(ServicoDeValidacaoDeCPF.CPFInvalidoException.class,
                () -> servicoDeValidacaoDeCPF.podeVotar(associado));
    }

    @Test
    @DisplayName("Quanto o serviço externo retorna 200 OK com status UNABLE_TO_VOTE, o serviço de validação retorna false")
    public void quandoServicoExternoRetornaComUNABLE_TO_VOTERetornaFalse() {
        MockRestServiceServer server = getMockRestServiceServer();
        server.expect(requestTo("/06646483477"))
                .andRespond(withSuccess().body("{\"status\":\"UNABLE_TO_VOTE\"}").contentType(MediaType.APPLICATION_JSON));

        Associado associado = Associado.novoAssociado("Nome", "066.464.834-77");

        assert !servicoDeValidacaoDeCPF.podeVotar(associado);
    }

    @Test
    @DisplayName("Quanto o serviço externo retorna 200 OK com status ABLE_TO_VOTE, o serviço de validação retorna true")
    public void quandoServicoExternoRetornaComABLE_TO_VOTERetornaTrue() {
        MockRestServiceServer server = getMockRestServiceServer();
        server.expect(requestTo("/06646483477"))
                .andRespond(withSuccess().body("{\"status\":\"ABLE_TO_VOTE\"}").contentType(MediaType.APPLICATION_JSON));

        Associado associado = Associado.novoAssociado("Nome", "066.464.834-77");

        assert servicoDeValidacaoDeCPF.podeVotar(associado);
    }

    @Test
    @DisplayName("Quanto o serviço externo retorna 200 OK mas com um status desconhecido (exemplificado por " +
                 "STATUS_INVALIDO), o serviço de validação lança StatusDesconhecidoException")
    public void quandoServicoExternoRetornaStatusInvalidoStatusDesconhecidoExceptionELancada() {
        MockRestServiceServer server = getMockRestServiceServer();
        server.expect(requestTo("/06646483477"))
                .andRespond(withSuccess().body("{\"status\":\"STATUS_INVALIDO\"}").contentType(MediaType.APPLICATION_JSON));

        Associado nome = Associado.novoAssociado("Nome", "066.464.834-77");

        Assertions.assertThrows(ServicoDeValidacaoDeCPF.StatusDesconhecidoException.class,
                () -> servicoDeValidacaoDeCPF.podeVotar(nome));
    }

    private MockRestServiceServer getMockRestServiceServer() {
        MockServerRestTemplateCustomizer customizer = new MockServerRestTemplateCustomizer();
        customizer.customize(servicoDeValidacaoDeCPF.getRestTemplate());
        return customizer.getServer(servicoDeValidacaoDeCPF.getRestTemplate());
    }

    @TestConfiguration
    static class ContextConfiguration {
        @Bean
        public ServicoDeValidacaoDeCPF servicoDeValidacaoDeCPF(RestTemplateBuilder restTemplateBuilder) {
            return new ServicoDeValidacaoDeCPF(restTemplateBuilder);
        }

    }
}
