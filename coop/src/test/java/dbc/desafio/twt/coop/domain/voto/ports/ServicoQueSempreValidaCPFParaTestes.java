package dbc.desafio.twt.coop.domain.voto.ports;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@TestConfiguration
public class ServicoQueSempreValidaCPFParaTestes {
    @Bean
    @Profile("test")
    public VerificarSeAssociadoPodeVotar verificarSeAssociadoPodeVotar() {
        return associado -> true;
    }
}
