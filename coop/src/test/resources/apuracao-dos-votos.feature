# language: pt
@txn
Funcionalidade: Contabilizar os votos e dar o resultado da votação em pautas

  Cenário de Fundo:
    Dados os associados
      | Nome      | CPF do Associado |
      | José      | 245.937.730-40   |
      | João      | 004.311.220-08   |
      | Maria     | 320.658.770-06   |
      | Ana       | 061.363.150-17   |
      | Antônio   | 494.340.550-91   |
      | Francisco | 976.076.380-08   |
      | Antônia   | 894.155.500-07   |
      | Adriana   | 070.293.190-01   |

  Cenário: Não é possível contabilizar os votos de uma sessão ainda aberta
    Dada uma nova pauta "Ordem 66" é cadastrada
    Quando a sessão da pauta "Ordem 66" for aberta
    E a sessão da pauta "Ordem 66" está aberta
    Então não é possível contabilizar os votos da pauta "Ordem 66"

  Delineação do Cenário: Uma pauta recebe um único voto
    Dada uma nova pauta "Ordem 66" é cadastrada
    Quando a sessão da pauta "Ordem 66" for aberta
    E a associada com CPF 320.658.770-06 lançar seu voto "<Voto>" na pauta "Ordem 66"
    E a sessão da pauta "Ordem 66" for encerrada
    Então o total de votos favoráveis da pauta "Ordem 66" é <Quantidade de votos favoráveis>
    E o total de votos desfavoráveis da pauta "Ordem 66" é <Quantidade de votos desfavoráveis>

    Exemplos:
      | Voto | Quantidade de votos favoráveis | Quantidade de votos desfavoráveis |
      | Sim  | 1                              | 0                                 |
      | Não  | 0                              | 1                                 |

  Cenário: Uma pauta recebe maioria de votos favoráveis
    Dada uma nova pauta "Ordem 66" é cadastrada
    Quando a sessão da pauta "Ordem 66" for aberta
    E os votos são lançados na pauta "Ordem 66"
      | Voto | CPF do Associado |
      | Sim  | 245.937.730-40   |
      | Não  | 004.311.220-08   |
      | Não  | 320.658.770-06   |
      | Sim  | 061.363.150-17   |
      | Sim  | 494.340.550-91   |
      | Sim  | 976.076.380-08   |
      | Não  | 894.155.500-07   |
      | Sim  | 070.293.190-01   |
    E a sessão da pauta "Ordem 66" for encerrada
    Então o total de votos favoráveis da pauta "Ordem 66" é 5
    E o total de votos desfavoráveis da pauta "Ordem 66" é 3

  Cenário: Uma pauta recebe a mesma quantidade de votos favoráveis e desfavoráveis
    Dada uma nova pauta "Ordem 66" é cadastrada
    Quando a sessão da pauta "Ordem 66" for aberta
    E os votos são lançados na pauta "Ordem 66"
      | Voto | CPF do Associado |
      | Sim  | 245.937.730-40   |
      | Não  | 004.311.220-08   |
      | Não  | 320.658.770-06   |
      | Sim  | 061.363.150-17   |
      | Sim  | 494.340.550-91   |
      | Sim  | 976.076.380-08   |
      | Não  | 894.155.500-07   |
      | Não  | 070.293.190-01   |
    E a sessão da pauta "Ordem 66" for encerrada
    Então o total de votos favoráveis da pauta "Ordem 66" é 4
    E o total de votos desfavoráveis da pauta "Ordem 66" é 4
