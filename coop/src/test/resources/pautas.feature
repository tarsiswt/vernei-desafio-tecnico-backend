# language: pt
@txn
Funcionalidade: Cadastrar nova pauta

  Cenário: uma nova pauta é cadastrada
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    Então a sessão da pauta "Eleição dos representantes" está fechada
    E a pauta recebe uma identificação única

  Cenário: duas pautas diferentes podem ter a mesma descrição
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    Quando uma nova pauta "Eleição dos representantes" é cadastrada
    Então as pautas "Eleição dos representantes" possuem identificações distintas
