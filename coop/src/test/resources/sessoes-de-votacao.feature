# language: pt
@txn
Funcionalidade: Associados votam para tomada de decisão em assembleias.

  Cenário: Um associado vota em uma pauta cuja sessão ainda não está aberta
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    E o associado "Társis Toledo" com CPF 066.464.834-77
    E a sessão da pauta "Eleição dos representantes" está fechada
    Quando o associado com CPF 066.464.834-77 lançar seu voto "Não" na pauta "Eleição dos representantes"
    Então o voto do associado com CPF 066.464.834-77 na pauta "Eleição dos representantes" não é contabilizado

  Cenário: Um associado vota favoravelmente em uma pauta cuja sessão está aberta
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    E o associado "Társis Toledo" com CPF 066.464.834-77
    Quando a sessão da pauta "Eleição dos representantes" for aberta
    E o associado com CPF 066.464.834-77 lançar seu voto "Sim" na pauta "Eleição dos representantes"
    Então o voto do associado com CPF 066.464.834-77 na pauta "Eleição dos representantes" é contabilizado

  Cenário: Um associado vota desfavoravelmente em pauta cuja sessão está aberta
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    E o associado "Társis Toledo" com CPF 066.464.834-77
    Quando a sessão da pauta "Eleição dos representantes" for aberta
    E o associado com CPF 066.464.834-77 lançar seu voto "Não" na pauta "Eleição dos representantes"
    Então o voto do associado com CPF 066.464.834-77 na pauta "Eleição dos representantes" é contabilizado

  Cenário: Um associado tenta votar duas vezes na mesma pauta
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    E o associado "Társis Toledo" com CPF 066.464.834-77
    Quando a sessão da pauta "Eleição dos representantes" for aberta
    E o associado com CPF 066.464.834-77 lançar seu voto "Não" na pauta "Eleição dos representantes"
    E o associado com CPF 066.464.834-77 lançar seu voto "Não" na pauta "Eleição dos representantes"
    Então apenas um voto do associado com CPF 066.464.834-77 na pauta "Eleição dos representantes" é contabilizado

  Cenário: Um associado tenta votar em uma pauta cuja sessão já foi encerrada
    Dado que uma nova pauta "Eleição dos representantes" é cadastrada
    E o associado "Társis Toledo" com CPF 066.464.834-77
    E a sessão da pauta "Eleição dos representantes" for aberta
    Quando a sessão da pauta "Eleição dos representantes" for encerrada
    E o associado com CPF 066.464.834-77 lançar seu voto "Sim" na pauta "Eleição dos representantes"
    Então o voto do associado com CPF 066.464.834-77 na pauta "Eleição dos representantes" não é contabilizado
