import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

import scala.concurrent.duration.FiniteDuration

class ApiGatlingSimulationTest extends Simulation {

  val scenarioBuilder = scenario("CriarPauta").repeat(10, "n") {
    val criarPauta = http("CriarPauta")
      .post("http://localhost:8085/pauta")
      .header("Content-Type", "application/json")
      .header("Accept", "application/vnd.dbc.coop.api-v1+json")
      .body(StringBody("""{"descricao":"Pauta${n}"}"""))
      .check(status.is(200))
      .check(jsonPath("$.id").saveAs("pautaId"))

    val abrirSessao = http("AbrirSessao")
      .post("http://localhost:8085/pauta/${pautaId}/abrirSessao")
      .header("Content-Type", "application/json")
      .header("Accept", "application/vnd.dbc.coop.api-v1+json")
      .body(StringBody("""{"duracao":"PT1M"}"""))
      .check(status.is(200))

    val criarAssociado = http("CriarAssociado")
      .post("http://localhost:8085/associado")
      .header("Content-Type", "application/json")
      .header("Accept", "application/vnd.dbc.coop.api-v1+json")
      .body(StringBody("""{"nome":"Associado${n}", "CPF":"066.464.834-77"}"""))
      .check(status.is(200))
      .check(jsonPath("$.id").saveAs("associadoId"))

    val receberVoto = http("ReceberVoto")
      .post("http://localhost:8085/pauta/${pautaId}/receberVoto")
      .header("Content-Type", "application/json")
      .header("Accept", "application/vnd.dbc.coop.api-v1+json")
      .body(StringBody("""{"associadoId": "${associadoId}", "voto": "true"}"""))

    exec(criarPauta).exec(abrirSessao).exec(criarAssociado).exec(receberVoto)
  }

  setUp(scenarioBuilder.inject(atOnceUsers(10))).maxDuration(FiniteDuration.apply(10, "minutes"))
}
